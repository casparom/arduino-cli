#!/bin/sh

ARDUINO_DIR=/root/Arduino
PERSISTENT_DIR=$(dirname ${CI_PROJECT_DIR})/shared/Arduino
VOLUME=$PERSISTENT_DIR:$ARDUINO_DIR


docker build -t casparom/arduino-cli .
docker run --rm -v $VOLUME casparom/arduino-cli sketch new Test
docker run --rm -v $VOLUME casparom/arduino-cli compile --fqbn esp32:esp32:esp32 ./Test
docker run --rm -v $VOLUME casparom/arduino-cli compile --fqbn esp8266:esp8266:arduino-esp8266 ./Test

if [ ! -f $PERSISTENT_DIR/Test/Test.esp32.esp32.esp32.bin ]; then
    echo "Compilation with esp32:esp32:esp32 failed"
    exit 1
elif [ ! -f $PERSISTENT_DIR/Test/Test.esp8266.esp8266.arduino-esp8266.bin ]; then
    echo "Compilation with esp8266:esp8266:arduino-esp8266 failed"
    exit 1
else
	echo "Test successful"
	ls $PERSISTENT_DIR/Test
	exit 0
fi