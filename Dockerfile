FROM jeanblanchard/alpine-glibc

ARG CLI_DIR=/usr/Arduino/cli
ARG CLI_EXE=$CLI_DIR/arduino-cli

WORKDIR /root/Arduino

RUN apk --update --no-cache add curl python libstdc++
RUN mkdir -p $CLI_DIR && \
	curl -L https://downloads.arduino.cc/arduino-cli/nightly/arduino-cli_nightly-latest_Linux_64bit.tar.gz | tar xz -C $CLI_DIR && \
	chmod +x $CLI_EXE && \
	curl https://bootstrap.pypa.io/get-pip.py | python && \
	pip install pyserial

COPY ./config /root/.arduino15

RUN $CLI_EXE core update-index
RUN $CLI_EXE core install esp32:esp32 -v
RUN $CLI_EXE core install esp8266:esp8266

ENTRYPOINT ["/usr/Arduino/cli/arduino-cli"]